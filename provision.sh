#!/bin/sh

[ -z "$GHUSER"] && echo "\$GHUSER" is not set && exit 1

apt-get update
apt-get -y install dirmngr
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" >> \
	/etc/apt/sources.list
apt-get update
apt-get -y install ansible git ca-certificates

git clone https://gitlab.com/iMil/autopimp.git
cd autopimp && \
	printf "[local]\nlocalhost ansible_connection=local\n" > hosts
	ansible-playbook -i hosts -l local single/essentials.yml --extra-vars "myuser=vagrant myghuser=$GHUSER"

echo "en_US.UTF-8 UTF-8" >/etc/locale.gen
LC_ALL=en_US.UTF-8 locale-gen

chsh -s /usr/bin/zsh vagrant
