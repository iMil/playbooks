# AutoPimp my shell!

Here you will find my personal shell customizations, they include:

- My essential packages (`vim`, `sudo`, `tmux`, `git`, `zsh`)
- User add
- `dotfiles` fetch and link
- `zsh` pimping with `oh-my-zsh` and `powerlevel9k`
- `vim` plugins installation
- `tmux` customization
- `ssh` public keys provisioning with _gitlab_'s public URL

Before running the `playbook` I like to check if it doesn't break anything, here's how to launch the container (available on [dockerhub](ihttps://cloud.docker.com/u/imil/repository/docker/imil/debansibletest/)):

```sh
$ docker run -it --rm -v $(pwd):/ansible imil/debansibletest:1.1 single/essentials.yml --extra-vars "myuser=foo myghuser=iMil new=yes testme=yes"
```
`--extra-vars`:

* `myuser` is the user you want to create and operate on
* `myghuser` is the _gitlab_ _username_ in order to fetch public keys.
* if `new=yes` is specified, this is a brand new install and the user does not exists, it will be created and granted `sudo` privileges. Otherwise, only shell modifications will be applied.
* if `testme=yes` is specified, the `playbook` will pause in order for you to check if the modifications went well. Enter the container this way:

```sh
$ docker exec -it -e "TERM=xterm-256color" db3ed1931a02 su - foo
```

If all went well you can `^C` the `playbook`.

Now in order to apply the playbook locally you'll need to create a `hosts` file containing the following:

```
[local]
localhost ansible_connection=local
```

The `playbook` has been tested on _Debian GNU/Linux_, _Linux Mint_, _NetBSD_, _FreeBSD_ and _Mac OS X_. In oder to change the target, as packages names and some tools change over OSes, simply `ln -s myos local` in `group_vars`. By default `local` points to `debian`.

Now launch the `playbook` locally on the target machine:

```sh
$ ansible-playbook -i hosts -K -l local single/essentials.yml --extra-vars "myuser=username myghuser=GitLabUserName"
```

Add `new=yes` if the user needs to be created.

For a bit more _pimpiness_, there's also `single/newtools.yml` which will install (Debian GNU/Linux and derivatives only):

- https://github.com/BurntSushi/ripgrep
- https://github.com/sharkdp/fd
- https://github.com/sharkdp/bat
- https://github.com/ogham/exa
- https://github.com/junegunn/fzf

And should be called like this with the actual user:

```sh
$ ansible-playbook -i hosts -l local single/newtools.yml
```
